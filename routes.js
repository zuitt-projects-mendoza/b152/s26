//Nodejs simple server
//require http method and save it an variable called http.
//use createServer() method from http to create our server.
//add an anonymous function in create server able to receive our res and req.
//add .listen() and assign our server to a port number.

let http = require("http");

http.createServer(function(req,res){

	res.writeHead(200,{'Content-Type': 'text/plain'});
	res.end('Hello from our second server!')

}).listen(8000);

console.log('Server is running from localhost:8000');
